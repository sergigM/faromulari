package com.example.form;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity3 extends AppCompatActivity {

    private Button botonShare;
    private Button botonMostrar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getIntent().getExtras();

        final String nom = bundle.getString("nombre");
        final int opcio = bundle.getInt("boton");
        final int edat = bundle.getInt("edat");

        setContentView(R.layout.activity_main3);
        botonMostrar = findViewById(R.id.mostrar);
        botonShare = findViewById(R.id.share);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        final String texto = "";

        botonMostrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (opcio == 1) {
                    Toast.makeText(MainActivity3.this, "Hola " + nom + ", com portes aquests " + edat + " anys?", Toast.LENGTH_LONG).show();
                    botonMostrar.setVisibility(View.INVISIBLE);

                } else if (opcio == 2) {
                    Toast.makeText(MainActivity3.this, "Espero tornar a veure't " + nom + ", abans que facis " + (edat + 1) + " anys", Toast.LENGTH_LONG).show();
                    botonMostrar.setVisibility(View.INVISIBLE);
                }
            }
        });

        botonShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent share = new Intent();
                share.setAction(Intent.ACTION_SEND);

                share.setType("text/plain");
                if (opcio == 1) {
                    Intent.createChooser(share,"Share via");

                    share.putExtra(Intent.EXTRA_TEXT, "Hola " + nom + ", com portes aquests " + edat + " anys?");

                } else if (opcio == 2) {
                    share.putExtra(Intent.EXTRA_TEXT, ("Espero tornar a veure't " + nom + ", abans que facis " + (edat + 1) + " anys"));
                }

                startActivity(Intent.createChooser(share,"Share via"));
            }
        });
    }
}