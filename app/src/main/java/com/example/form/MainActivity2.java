package com.example.form;


import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity2 extends AppCompatActivity {
    private Button buttonNext;
    private SeekBar barra;
    private RadioButton button1;
    private RadioButton button2;
    private RadioGroup radioGrup;
    private TextView textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Bundle bundle = getIntent().getExtras();

        final String nom = bundle.getString("nombre");


        barra = findViewById(R.id.barra);
        barra.setProgress(0);
        barra.setMax(100);

        buttonNext = findViewById(R.id.buttonNext);
        button1 = findViewById(R.id.hola);
        button2 = findViewById(R.id.adeu);
        textView = findViewById(R.id.edad);


        buttonNext.setVisibility(View.INVISIBLE);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        barra.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar barra, int progress, boolean fromUser) {
                textView.setText(String.valueOf(progress));

            }

            public void onStartTrackingTouch(SeekBar barra) {

            }

            public void onStopTrackingTouch(SeekBar barra) {
                if (Integer.parseInt(textView.getText().toString()) < 18 || 60<Integer.parseInt(textView.getText().toString())) {
                    Toast.makeText(MainActivity2.this, "No Podés Continuar: Menor de 18 o Mayor de 60", Toast.LENGTH_SHORT).show();
                    buttonNext.setVisibility(View.INVISIBLE);
                }else{
                    buttonNext.setVisibility(View.VISIBLE);
                }
            }

        });


        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int edat = Integer.parseInt(textView.getText().toString());

                Intent activity3 = new Intent(MainActivity2.this, MainActivity3.class);
                activity3.putExtra("nombre", nom);
                activity3.putExtra("edat", edat);
                if (button1.isChecked()) {
                    activity3.putExtra("boton", 1);
                    startActivity(activity3);
                }
                else if (button2.isChecked()) {
                    activity3.putExtra("boton", 2);
                    startActivity(activity3);
                }
                else if(!button1.isChecked()||!button2.isChecked()){
                    Toast.makeText(MainActivity2.this, "No Podés Continuar", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}