package com.example.form;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText editTextNom;
    private Button buttonNext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonNext = findViewById(R.id.buttonNext);
        editTextNom = findViewById(R.id.nom);

        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nom = editTextNom.getText().toString();
                if (nom.isEmpty()) {
                    Toast.makeText(MainActivity.this, "Introdueix un nom", Toast.LENGTH_LONG).show();
                } else {
                    Intent activity2 = new Intent(MainActivity.this, MainActivity2.class);
                    activity2.putExtra("nombre", nom);
                    startActivity(activity2);
                }
            }
        });

        editTextNom.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == 66) {
                    String nom = editTextNom.getText().toString();
                    if (nom.isEmpty()) {
                        Toast.makeText(MainActivity.this, "Introdueix un nom", Toast.LENGTH_LONG).show();
                    } else {
                        Intent activity2 = new Intent(MainActivity.this, MainActivity2.class);
                        activity2.putExtra("nombre", nom);
                        startActivity(activity2);
                    }
                }
                return false;
            }
        });
    }
}